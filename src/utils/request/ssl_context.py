# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: ssl_context.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for configuring the SSL context
# ================================================================================================
import ssl
from ssl import SSLContext
from ssl import SSLError
from ssl import VerifyMode
from logger import Logger

class SslContextConfigurator:
    """
    Class for configuring the SSL context.
    """

    def __init__(self, logger=Logger):
        """
        Initializes the SslContextConfigurator class.

        Args:
            logger (logging.Logger): The logger to use for logging.
        """
        self.logger = logger()

    def create_context(self, check_hostname: bool, verify_mode: VerifyMode) -> SSLContext:
        """
        Creates an SSL context with the given parameters.

        Args:
            check_hostname (bool): If true, the hostname will be verified.
            verify_mode (VerifyMode): The SSL verification mode to use.

        Returns:
            SSLContext: The created SSL context.
        """
        try:
            context = ssl.create_default_context()
            context.check_hostname = check_hostname
            context.verify_mode = verify_mode
            return context
        except SSLError as e:
            self.logger.info(f"SSLError: {e.reason}")
            return None
