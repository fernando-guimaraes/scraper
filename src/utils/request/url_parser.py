# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: url_parser.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for validating the URL schema.
# ================================================================================================
from urllib.parse import ParseResult
from urllib.parse import urlparse
from logger import Logger


class ParserUrl:
    """
    Parser url for requests.
    """

    def __init__(self, url: str, logger=Logger) -> None:
        """_summary_

        Args:
            url (str): _description_
            logger (logging.Logger): _description_
        """
        self.url = url
        self.logger = logger()
        self.allowed_schemes = ["http", "https"]

    def parse_url(self) -> ParseResult:
        """
        Parses the url.

        Returns:
            ParseResult: returning a 6-item named tuple. This corresponds to the general structure of a URL.
        """

        try:
            return urlparse(self.url)
        except AttributeError as e:
            self.logger.error(f"Error in URL: {e}")
            return None

    def url_is_allowed(self) -> bool:
        """
        Checks if the url format is allowed.

        Returns:
            bool: Return if url is allowed.
        """
        parsed_url = self.parse_url()
        if parsed_url.scheme in self.allowed_schemes:
            return True
        return False
