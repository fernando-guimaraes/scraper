#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ================================================================================================
# Module: schemas.py
# Author: Fernando Theodoro Guimarães
# Description: This code is responsible for creating the schemas of each table.
# ================================================================================================
from pyspark.sql.types import DataType
from pyspark.sql.types import StructField
from pyspark.sql.types import StructType


class SchemaTables:
    """
    This class is responsible for creating the schemas of all tables
    """

    def __init__(self) -> None:
        self.schema = StructType()

    @property
    def get_schema(self) -> StructType:
        return self.schema

    @property
    def get_fields_name(self) -> list[str]:
        return self.schema.fieldNames()

    @staticmethod
    def get_fields(infos: any) -> tuple[DataType, bool, dict]:
        """
        Extracts the fields from the provided information.

        Args:
            infos (any): The information from which to extract the fields. This can be of any type.

        Returns:
            tuple[DataType, bool, dict]: A tuple containing the DataType, a boolean value, and a dictionary.
        """
        if isinstance(infos[0], tuple):
            return infos[0]
        return infos

    def create_spark_struct_type(self, table_infos: dict) -> None:
        """
        Creates a Spark StructType schema based on the provided table information.

        Args:
            table_infos (dict): A dictionary where each key is a column name and the value is a tuple containing the data type, nullable flag, and metadata for the column.
        """
        for column_name, infos in table_infos.items():
            data_type, nullable, metadata = self.get_fields(infos)
            field = StructField(column_name, data_type, nullable, metadata)
            self.get_schema.add(field)
