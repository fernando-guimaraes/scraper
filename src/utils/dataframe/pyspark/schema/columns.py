# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: columns.py
# Author: Fernando Theodoro Guimarães
# Description: This code centralizes all columns to use in spark schemas.
# ================================================================================================
from typing import NamedTuple

from pyspark.sql.types import FloatType
from pyspark.sql.types import IntegerType
from pyspark.sql.types import StringType
from pyspark.sql.types import TimestampType


class DataframeInfos(NamedTuple):
    schema_params: tuple
    regex: tuple


class Columns(enumerate):
    """
    Class to set all columns for schema on DataFrames.
    """

    STRING_TYPE = StringType()
    TIMESTAMP_TYPE = TimestampType()
    FLOAT_TYPE = FloatType()
    INTEGER_TYPE = IntegerType()

    TABLE_INFOS_CFC: dict = {
        "chave": DataframeInfos(
            schema_params=(STRING_TYPE, True, {"description": "Chave única para cada registro"}),
            regex=((r"[^\w\s]", r" ")),
        ),
        "nome": DataframeInfos(
            schema_params=(STRING_TYPE, True, {"description": "Nome do profissional"}),
            regex=((r"[^\w\s]", r"")),
        ),
        "registro": DataframeInfos(
            schema_params=(
                STRING_TYPE,
                True,
                {"description": "Número de registro do profissional"},
            ),
            regex=((r"[^\w\s]", r" ")),
        ),
        "estado_conselho": DataframeInfos(
            schema_params=(STRING_TYPE, True, {"description": "Estado do conselho profissional"}),
            regex=((r"[^\w\s]", r" ")),
        ),
        "cpf_cnpj": DataframeInfos(
            schema_params=(STRING_TYPE, True, {"description": "CPF ou CNPJ do profissional"}),
            regex=((r"[^\w\s]", r"")),
        ),
        "descricao_categoria": DataframeInfos(
            schema_params=(
                STRING_TYPE,
                True,
                {"description": "Descrição da categoria profissional"},
            ),
            regex=((r"[^\w\s]", r"")),
        ),
        "descricao_tipo_registro": DataframeInfos(
            schema_params=(STRING_TYPE, True, {"description": "Descrição do tipo de registro"}),
            regex=((r"[^\w\s]", r"")),
        ),
        "situacao_cadastral": DataframeInfos(
            schema_params=(
                STRING_TYPE,
                True,
                {"description": "Situação cadastral do profissional"},
            ),
            regex=((r"[^\w\s]", r"")),
        ),
    }

    INFO_CONTROL_COLUMNS: dict = {
        "arquivo": (STRING_TYPE, True, {"description": "Nome do arquivo de origem"}),
        "data_processamento": (
            TIMESTAMP_TYPE,
            True,
            {"description": "Data em que o processamento foi executado"},
        ),
    }
