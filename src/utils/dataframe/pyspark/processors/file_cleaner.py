# -*- coding: utf-8 -*-
#!/usr/bin/env python3
# ================================================================================================
# Module: cleaner.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for methods that apply treatments to PySpark DataFrames.
# ================================================================================================
from datetime import datetime

from logger import Logger
from pyspark.sql import DataFrame
from pyspark.sql.functions import col
from pyspark.sql.functions import lit
from pyspark.sql.functions import regexp_replace
from pyspark.sql.functions import to_timestamp
from pyspark.sql.functions import translate
from pyspark.sql.functions import trim
from pyspark.sql.functions import upper
from utils import Utils
from variables import Variables


class CleanTables:
    """
    A class that contains methods for cleaning PySpark DataFrames.
    """

    def __init__(self, logger=Logger, utils=Utils, variables=Variables) -> None:
        """
        Initializes the CleaningTables class.

        Args:
            logger (Logger): The logger to be used.
            utils (Utils): The utility functions to be used.
        """
        self.logger = logger()
        self.utils = utils()
        self.variables = Variables
        self.processing_date = datetime.now(self.variables.TIMEZONE_BR).strftime("%Y-%m-%d %H:%M:%S")

    def apply_replace_chars(self, df: DataFrame, column: str) -> DataFrame:
        """
        Replaces characters in a DataFrame column.

        Args:
            df (DataFrame): The DataFrame to be processed.
            column (str): The column in which characters are to be replaced.

        Returns:
            DataFrame: The processed DataFrame.
        """
        df = df.withColumn(
            column,
            trim(
                upper(
                    translate(
                        column,
                        self.variables.CHAR_WITH_ACCENTS,
                        self.variables.CHAR_WITHOUT_ACCENTS,
                    )
                )
            ),
        )
        return df

    def apply_regexp_replace(
        self, df: DataFrame, column: str, pattern: str, replacement: str
    ) -> DataFrame:
        """
        Applies a regular expression replacement to a DataFrame column.

        Args:
            df (DataFrame): The DataFrame to be processed.
            column (str): The column in which the replacement is to be applied.
            pattern (str): The pattern to be replaced.
            replacement (str): The string to replace the pattern with.

        Returns:
            DataFrame: The processed DataFrame.
        """
        df = df.withColumn(column, regexp_replace(col(column), pattern, replacement))
        return df

    def apply_all_transformations(
        self, df: DataFrame, column: str, replacement_patterns: any
    ) -> DataFrame:
        """
        Applies all transformations to a DataFrame column.

        Args:
            df (DataFrame): The DataFrame to be processed.
            column (str): The column to be transformed.
            replacement_patterns (any): The replacement patterns to be applied.

        Returns:
            DataFrame: The processed DataFrame.
        """
        df = self.apply_replace_chars(df, column)
        if isinstance(replacement_patterns.schema_params, str):
            pattern, replacement = replacement_patterns
            df = self.apply_regexp_replace(df, column, pattern, replacement)
            return df
        if isinstance(replacement_patterns.schema_params, tuple):
            for pattern, replacement in replacement_patterns:
                df = self.apply_regexp_replace(df, column, pattern, replacement)
                return df
        return None

    def adding_control_column(self, df: DataFrame, file_name: str) -> DataFrame:
        """
        Adding control columns in DataFrame.

        Args:
            df (DataFrame): The DataFrame to be processed.
            file_name (str): The file name to be  processed.

        Returns:
            DataFrame: The processed DataFrame.
        """
        df = df.withColumn("arquivo", lit(file_name))
        df = df.withColumn("data_processamento", lit(self.processing_date))
        df = df.withColumn("data_processamento", to_timestamp("data_processamento"))
        return df

    def process_df(self, df: DataFrame, table_infos: dict, file_name: str) -> DataFrame:
        """
        Processes a DataFrame based on provided column information.

        Args:
            df (DataFrame): The DataFrame to be processed.
            table_infos (dict): Information about the columns to be processed.
            file_name (str): The file name to be  processed.

        Returns:
            DataFrame: The processed DataFrame.
        """
        # This position "infos.regex" contains the replacement patterns to be applied.
        # (e.g. regexp_replace("[^\w\s]", ""))
        for column_name, infos in table_infos.items():
            df = self.apply_all_transformations(df, column_name, infos.regex)
        df = self.adding_control_column(df, file_name)
        return df
