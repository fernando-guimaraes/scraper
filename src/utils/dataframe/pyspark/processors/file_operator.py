# -*- coding: utf-8 -*-
#!/usr/bin/env python3
# ================================================================================================
# Module: handler.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for methods that apply operations to PySpark DataFrames.
# ================================================================================================
from logger import Logger
from pyspark.sql import DataFrame
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType


class PysparkHandler:
    """
    Handles operations related to PySpark.
    """

    def __init__(self, logger=Logger) -> None:
        """
        Initializes the PysparkHandler with a logger.

        Args:
            logger (Logger, optional): The logger to be used. Defaults to Logger.
        """
        self.logger = logger()

    def read_csv_file(
        self,
        spark: SparkSession,
        file_path: str,
        delimiter: str,
        header: bool,
        encoding: str,
        schema: StructType,
    ) -> DataFrame:
        """
        Reads a CSV file into a DataFrame using PySpark.

        Args:
            spark (SparkSession): The SparkSession to use.
            file_path (str): The path to the CSV file.
            delimiter (str): The delimiter used in the CSV file.
            header (bool): Whether the CSV file has a header.
            encoding (str): The encoding of the CSV file.
            schema (StructType): The schema to use when reading the CSV file.

        Returns:
            DataFrame: The DataFrame created from the CSV file.
        """
        try:
            df = (
                spark.read.option("delimiter", delimiter)
                .option("encoding", encoding)
                .csv(file_path, header=header, schema=schema)
            )
            return df
        except Exception as e:
            self.logger.error(e)
            return None

    def write_file(
        self,
        df: DataFrame,
        write_format: str,
        dest_path: str,
        mode: str,
        delimiter: str = None,
        header: bool = None,
        encoding: str = None,
    ) -> None:
        """
        Writes a DataFrame to a file in the specified format.

        Args:
            df (DataFrame): The DataFrame to write.
            write_format (str): The format to write the DataFrame in.
            dest_path (str): The destination path for the file.
            mode (str): The write mode (e.g., 'overwrite', 'append').
            delimiter (str, optional): The delimiter to use (if applicable). Defaults to None.
            header (bool, optional): Whether to write a header (if applicable). Defaults to None.
            encoding (str, optional): The encoding to use (if applicable). Defaults to None.
        """
        try:
            if write_format == "csv":
                df.write.mode(mode).options(
                    delimiter=delimiter, encoding=encoding, header=header
                ).csv(dest_path)
            if write_format == "parquet":
                df.write.mode(mode).parquet(dest_path)
        except Exception as e:
            self.logger.error(e)
