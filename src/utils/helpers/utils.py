# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: utils.py
# Author: Fernando Theodoro Guimarães
# Description: This code centralizes all methods to use in other modules
# ================================================================================================
from logger import Logger


class Utils:
    """
    A utility class that provides various helper methods.
    """

    def __init__(self, logger=Logger) -> None:
        """
        Initializes the Utils class.

        Args:
            logger (logging.Logger): The logger to be used for logging.
        """
        self.logger = logger()

    def check_variables(self, class_variables: callable) -> None:
        """
        Check if all variables defined in the Variables class have been initialized.

        Args:
            class_variables (callable): Instance of class Variables.
        """
        for key, value in class_variables.get_all_variables():
            if not value:
                self.logger.error(
                    f"The variable {key} was not created, because the value is: {value}"
                )
