# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: stopwatch.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for calculating the execution time of tasks.
# ================================================================================================
import time
from logger import Logger

class Stopwatch:
    """
    A Stopwatch class that can be used to measure the time of operations.
    """

    def __init__(self, logger=Logger) -> None:
        """Initializes the Stopwatch object.

        Args:
            logger (logging.Logger): The logger to be used for logging.
        """
        self.logger = logger()
        self.start_time = None
        self.end_time = None

    def start(self):
        """
        Start the Stopwatch.
        """
        self.start_time = time.perf_counter()

    def stop(self):
        """
        Stop the Stopwatch.
        """
        self.end_time = time.perf_counter()

    def elapsed_time(self):
        """
        Calculate the elapsed time.
        """
        return self.end_time - self.start_time

    def show_elapsed_time(self):
        """
        Show the elapsed time.
        """
        elapsed_time = self.elapsed_time()

        hours = round(elapsed_time // 3600, 2)
        minutes = round((elapsed_time % 3600) // 60, 2)
        seconds = round(elapsed_time % 60, 4)

        self.logger.info(f"Runtime execution: {hours} hours {minutes} minutes {seconds} seconds")
