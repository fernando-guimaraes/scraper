# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: upload_files_bucket.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for preparing the bucket partitions to create external tables via Terraform
# ================================================================================================
import os
import pandas as pd
from src.utils.gcp.storage.gcs_client import GcsClient
from src.utils.gcp.storage.gcs_manager import GcsManager
from src.utils.helpers.logger import Logger
from src.utils.helpers.variables import Variables
from src.utils.time.dates import CurrentDate


class Uploader(GcsClient, GcsManager):
    """
    This class is responsible for uploading files to Google Cloud Storage.

    Args:
        GcsClient (GcsClient): An instance of the GcsClient class.
        GcsManager (GcsManager): An instance of the GcsManager class.
    """

    def __init__(self, project_id: str, bucket_id: str, logger=Logger, variables=Variables) -> None:
        """
        Initializes the Uploader class with the given project ID, bucket ID, and logger.

        Args:
            project_id (str): The ID of the Google Cloud project.
            bucket_id (str): The ID of the Google Cloud Storage bucket.
            logger (logging.Logger): The logger to use for logging events.
        """
        self.project_id = project_id
        self.bucket_id = bucket_id
        self.logger = logger()
        self.variables = variables
        self.client = self.instantiate_client()
        self.bucket = self.create_bucket_obj(self.client)
        self.FILE_PATH = os.path.join(os.getcwd(), "tf.parquet")
        self.BLOB_TRUSTED_NAMES = (
            "conselho-federal-contabilidade/empresa",
            "conselho-federal-contabilidade/profissional",
        )
        GcsClient.__init__(self, project_id, bucket_id, logger)
        GcsManager.__init__(self, self.bucket, logger)

    def create_empty_df(self) -> None:
        """
        Creates an empty DataFrame and saves it as a Parquet file.
        """
        df = pd.DataFrame()
        df.to_parquet(self.FILE_PATH)

    def upload_parquet_to_trusted(self, current_dates: tuple) -> None:
        """
        Uploads a parquet file to a trusted blob path.

        Args:
            current_dates (tuple): The current dates in the format (year, month, day).
        """
        year, month, day = current_dates
        for blob_path in self.BLOB_TRUSTED_NAMES:
            blob_trusted_path = f"trusted/{blob_path}/ano={year}/mes={month}/dia={day}/tf.parquet"
            try:
                blob = self.create_blob(blob_trusted_path)
                self.blob_upload_from_file_name(blob, self.FILE_PATH)
                if self.check_if_blob_exists(blob):
                    self.logger.info(f"{blob.name} has been uploaded successfully.")
            except Exception as e:
                self.logger.error(e)


if __name__ == "__main__":
    logger = Logger()
    current_date = CurrentDate()
    dates = current_date.get_dates()

    BUCKET_ID, PROJECT_ID = (os.getenv(var) for var in ("BUCKET_ID", "PROJECT_ID"))

    logger.info("Uploading python files for scripts in Cloud Storage")
    uploader_storage = Uploader(PROJECT_ID, BUCKET_ID)
    # Create empty DataFrame for create of external tables
    uploader_storage.create_empty_df()

    logger.info("Uploading python files for Trusted in Cloud Storage")
    uploader_storage.upload_parquet_to_trusted(dates)

    logger.info("All files have been uploaded successfully.")
