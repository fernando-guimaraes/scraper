# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: conselho_federal_contabilidade.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for scraping data from the Conselho Federal de Contabilidade
# ================================================================================================
import asyncio
import ssl
import sys

import aiohttp
import nest_asyncio
from aiohttp import TCPConnector
from async_requests import AsyncRequest
from file_handler import PandasFileHandle
from gcs_client import GcsClient
from gcs_manager import GcsManager
from google.cloud import storage
from logger import Logger
from ssl_context import SslContextConfigurator
from stopwatch import Stopwatch
from strategy import Strategy
from variables import Variables


class ScraperCfc(Strategy):
    """
    Class responsible for scraping concrete CFC data.
    """

    def __init__(
        self,
        type_list: str,
        url: str,
        bucket: storage.Bucket,
        logger=Logger,
        gcs_manager=GcsManager,
        pandas_file_handle=PandasFileHandle,
        async_request=AsyncRequest,
        variables=Variables
    ) -> None:
        """
        Initializes the ConcreteScraperCfc class.

        Args:
            type_list (str): Type of list to be scraped. Must be "Profissional" or "Empresa".
            url (str): URL of the website to be scraped.
            logger (Logger, optional): The logger to be used for logging.
            pandas_file_handle (PandasFileHandle, optional): Handler to handle files operations. Defaults to PandasFileHandle.
            async_request (AsyncRequest, optional): Asynchronous request to make network calls. Defaults to AsyncRequest.
            utils (Utils, optional): A utility class that provides various helper methods. Defaults to Utils.

        Raises:
            ValueError: Raises an error if `type_list` is not "Profissional" or "Empresa".
        """
        if type_list not in ["Profissional", "Empresa"]:
            raise ValueError("type_list must be Profissional or Empresa")
        self.type_list = type_list
        self.url = url
        self.bucket = bucket
        self.logger = logger()
        self.gcs_manager = gcs_manager(self.bucket)
        self.async_request = async_request
        self.pandas_file_handle = pandas_file_handle()
        self.variables = variables
        self.SKIP, self.TAKE = (0, 0)

    def create_blob_staging_path(self, uf: str) -> str:
        """
        Create a blob path for a specific file in Google Cloud Storage.

        Args:
            uf (str): The unique identifier for the file.

        Returns:
            str: The blob path in Google Cloud Storage.
        """
        layer_name = "staging"
        dir_name = f"{self.variables.DIR_NAME_CFC}/{self.type_list.lower()}"
        file_name = self.variables.FILENAME.format(uf.lower())
        blob_name = self.variables.BLOB_NAME.format(layer_name, dir_name, file_name)
        return blob_name

    def process_df_from_json(self, data: dict, uf: str, blob_name: str) -> None:
        """
        Processes a DataFrame from a JSON object and saves it to a CSV file.

        Args:
            data (dict): The data to be processed.
            uf (str): The state to process the response for.
            blob_name (str): The path where the file will be saved in bucket.
        """
        self.logger.info("Reading as DataFrame from JSON response.")
        df = self.pandas_file_handle.normalize_json_file(data)

        self.logger.info(f"Total number of records for {uf}: {df.shape[0]}")
        csv_data = self.pandas_file_handle.convert_to_csv_file(df=df)

        self.logger.info("Uploading CSV files in the bucket.")
        blob = self.gcs_manager.create_blob(self.bucket, blob_name)
        self.gcs_manager.blob_upload_from_string(self.bucket, blob, csv_data)
        self.logger.info(f"File were uploaded successfully to GCS: {blob.name}")

    async def process_response(self, session: aiohttp.ClientSession, uf: str) -> None:
        """
        Processes the response for a given session and state.

        Args:
            session (aiohttp.ClientSession): The session to process the response for.
            uf (str): The state to process the response for.
        """
        url = self.url.format(self.type_list, uf, self.SKIP, self.TAKE)
        request = self.async_request(session, url, self.logger)
        json_response = await request.fetch_json_response()
        results = json_response.get("data")
        blob_path = self.create_blob_staging_path(uf)
        self.process_df_from_json(results, uf, blob_path)
        await asyncio.sleep(1)

    async def scraper(self, connector: TCPConnector) -> None:
        """
        Main method that runs all tasks for all states.
        """
        async with aiohttp.ClientSession(connector=connector, headers=self.variables.HEADERS) as session:
            tasks = [self.process_response(session, uf) for uf in self.variables.UF]
            await asyncio.gather(*tasks)


if __name__ == "__main__":
    logger = Logger()

    stopwatch = Stopwatch()
    stopwatch.start()

    nest_asyncio.apply()

    logger.info("Starting extraction process for Professional.")

    BUCKET_ID = sys.argv[2]
    PROJECT_ID = sys.argv[4]

    gcs_client = GcsClient(PROJECT_ID, BUCKET_ID)
    client = gcs_client.instantiate_client()
    bucket = gcs_client.create_bucket_obj(client)

    ssl_context_configurator = SslContextConfigurator()
    ssl_context = ssl_context_configurator.create_context(True, ssl.CERT_REQUIRED)
    connector = TCPConnector(ssl=ssl_context)

    scraper_pf = ScraperCfc("Profissional", Variables.URL_CFC, bucket)
    asyncio.run(scraper_pf.scraper(connector))

    logger.info("Starting extraction process for Enterprise.")
    scraper_pj = ScraperCfc("Empresa", Variables.URL_CFC, bucket)
    asyncio.run(scraper_pj.scraper(connector))

    stopwatch.stop()
    stopwatch.show_elapsed_time()
