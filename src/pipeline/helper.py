# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: helper.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for supporting the transformation and ingestion modules.
# ================================================================================================
import sys

from builder import SchemaTables
from columns import Columns
from file_cleaner import CleanTables
from file_operator import PysparkHandler
from gcs_client import GcsClient
from gcs_manager import GcsManager
from google.cloud import storage
from logger import Logger
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType
from sparksession import SparkLauncher


class PipelineUtils(SchemaTables, CleanTables, PysparkHandler):
    """
    A PipelineUtils class that inherits from PysparkHandler.
    """

    def __init__(self, logger=Logger, gcs_client=GcsClient, gcs_manager=GcsManager) -> None:
        """
        Initializes the PipelineUtils class.

        Args:
            logger: Description of the logger parameter. Defaults to Logger.
            gcs_client: Description of the gcs_client parameter. Defaults to GcsClient.
            gcs_manager: Description of the gcs_manager parameter. Defaults to GcsManager.
        """
        SchemaTables.__init__(self)
        CleanTables.__init__(self)
        PysparkHandler.__init__(self)
        self.logger = logger()
        self.gcs_client = gcs_client
        self.gcs_manager = gcs_manager

    @property
    def get_schema(self) -> StructType:
        """
        Gets the schema.

        Returns:
            StructType: Description of the return value.
        """
        return self.schema

    @property
    def getenv_vars(self) -> tuple[str]:
        """
        Gets the environment variables.

        Returns:
            tuple[str]: Description of the return value.
        """
        return (sys.argv[i] for i in range(1, 7))

    def init_sparksession(
        self, master: str, app_name: str, bucket_id: str, dataset_id: str, project_id: str
    ) -> SparkSession:
        """
        Initializes the Spark session.

        Args:
            master (str): Description of the master parameter.
            app_name (str): Description of the app_name parameter.
            bucket_id (str): Description of the bucket_id parameter.
            dataset_id (str): Description of the dataset_id parameter.
            project_id (str): Description of the project_id parameter.

        Returns:
            SparkSession: Description of the return value.
        """
        spark_launcher = SparkLauncher(
            master, app_name, bucket_id, dataset_id, project_id, self.logger
        )
        spark = spark_launcher.initialize_sparksession()
        return spark

    def get_bucket_obj(self, project_id: str, bucket_id: str) -> storage.Bucket:
        """
        Gets the bucket object.

        Args:
            project_id (str): Description of the project_id parameter.
            bucket_id (str): Description of the bucket_id parameter.

        Returns:
            storage.Bucket: Description of the return value.
        """
        gcs_client = self.gcs_client(project_id, bucket_id, self.logger)
        client = gcs_client.instantiate_client()
        bucket = gcs_client.create_bucket_obj(client)
        return bucket

    def get_list_blobs(self, bucket: storage.Bucket, dir_name: str) -> list[str]:
        """
        Gets the list of blobs.

        Args:
            bucket (storage.Bucket): Description of the bucket parameter.
            dir_name (str): Description of the dir_name parameter.

        Returns:
            list[str]: Description of the return value.
        """
        gcs_manager = self.gcs_manager(bucket, self.logger)
        blobs = gcs_manager.get_list_blobs(dir_name)
        return blobs

    def upload_blob(
        self, bucket: storage.Bucket, blob: storage.Blob, destination_path: str
    ) -> None:
        """
        Uploads a blob.

        Args:
            bucket (storage.Bucket): Description of the bucket parameter.
            blob (storage.Blob): Description of the blob parameter.
            destination_path (str): Description of the destination_path parameter.
        """
        gcs_manager = self.gcs_manager(bucket, self.logger)
        gcs_manager.blob_upload_from_file(blob, destination_path)

    @staticmethod
    def get_source_path(bucket_id: str, blob: storage.Blob) -> str:
        """
        Gets the source path.

        Args:
            bucket_id (str): Description of the bucket_id parameter.
            blob (storage.Blob): Description of the blob parameter.

        Returns:
            str: Description of the return value.
        """
        return f"gs://{bucket_id}/{blob.name}"

    def df_is_valid(self, df):
        """
        Checks if the DataFrame is not empty.

        Args:
            df (pyspark.sql.DataFrame): The DataFrame to check.

        Returns:
            bool: Returns True if the DataFrame is not empty, otherwise it logs an error and returns False.
        """
        if not df.rdd.isEmpty():
            return True
        self.logger.error(f"The DataFrame is empty: {df.show()}")
        return False

    def build_df_schema(self, table_infos: dict) -> None:
        """
        Builds the dataframe schema.

        Args:
            table_infos (dict): Description of the table_infos parameter.
        """
        # Creating spark schemas from columns.
        self.create_spark_struct_type(table_infos)
        # Adding control columns to the schema.
        self.create_spark_struct_type(Columns.INFO_CONTROL_COLUMNS)
