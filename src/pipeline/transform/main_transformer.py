# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: main_transformer.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for executing the ingestion task.
# ================================================================================================
from google.cloud import storage
from helper import PipelineUtils
from logger import Logger
from pyspark.sql import SparkSession
from stopwatch import Stopwatch
from variables import Variables


class Transformer(PipelineUtils):
    """
    A Transformer class that inherits from PipelineUtils.
    """

    def __init__(self, logger=Logger, variables=Variables) -> None:
        """
        Initializes the Transformer class.

        Args:
            logger: Description of the logger parameter. Defaults to Logger.
        """
        PipelineUtils.__init__(self)
        self.logger = logger()
        self.variables = variables

    @staticmethod
    def get_file_name(blob: storage.Blob) -> str:
        """
        Gets the file name from the blob.

        Args:
            blob (storage.Blob): The blob from which to extract the file name.

        Returns:
            str: The extracted file name.
        """
        return blob.name.split("/")[-1]

    def transform_all_files(
        self,
        spark: SparkSession,
        bucket_id: str,
        blobs: list,
        table_infos: dict,
        df_infos: tuple,
    ) -> None:
        """
        Transforms all files in the given list of blobs.

        Args:
            spark (SparkSession): The SparkSession to use.
            bucket_id (str): The ID of the bucket where the blobs are located.
            blobs (list): The list of blobs to transform.
            table_infos (dict): Information about the tables.
            df_infos (tuple): Information about the dataframes.

        """
        write_format, delimiter, header, encoding, mode = df_infos
        for blob in blobs:
            if blob.exists():
                source_path = self.get_source_path(bucket_id, blob)
                file_name = self.get_file_name(blob)
                # Read CSV file in transient layer.
                df = self.read_csv_file(
                    spark, source_path, delimiter, header, encoding, self.get_schema
                )
                if self.df_is_valid(df):
                    # Processing DataFrame for transient layer.
                    df = self.process_df(df, table_infos, file_name)
                    dest_path = source_path.replace("staging", "transient")[:-4]
                    # Write a blob into transient layer.
                    self.write_file(df, write_format, dest_path, mode, delimiter, header, encoding)

    def main(self) -> None:
        """
        The main method of the Transformer class.
        """
        STEP_ID, BUCKET_ID, DATASET_ID, PROJECT_ID, APP_NAME, MASTER = self.getenv_vars
        infos = self.variables.TRANSFORMER_TASKS.get(STEP_ID)

        dir_name, table_infos, df_params = (infos.dir_name, infos.table_infos, infos.df_params)
        spark = self.init_sparksession(MASTER, APP_NAME, BUCKET_ID, DATASET_ID, PROJECT_ID)

        self.logger.info("Searching for files for processing.")
        bucket = self.get_bucket_obj(PROJECT_ID, BUCKET_ID)
        blobs = self.get_list_blobs(bucket, f"staging/{dir_name}")

        self.logger.info("Building Spark Schema for DataFrames.")
        self.build_df_schema(table_infos)

        self.logger.info(f"Running data transformations for: gs://{BUCKET_ID}/transient/{dir_name}")
        self.transform_all_files(spark, BUCKET_ID, blobs, table_infos, df_params)

        spark.stop()
        self.logger.info("The SparkSession has been stopped.")


if __name__ == "__main__":
    logger = Logger()

    stopwatch = Stopwatch()
    stopwatch.start()

    logger.info("Starting transformation process.")
    transformer = Transformer()
    transformer.main()

    stopwatch.stop()
    stopwatch.show_elapsed_time()
