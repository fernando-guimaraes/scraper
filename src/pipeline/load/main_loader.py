# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: main_loading.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for executing the ingestion task.
# ================================================================================================
from helper import PipelineUtils
from logger import Logger
from pyspark.sql import SparkSession
from stopwatch import Stopwatch
from variables import Variables


class Loader(PipelineUtils):
    """
    A Loader class that inherits from PysparkHandler.
    """

    def __init__(self, logger=Logger, variables=Variables) -> None:
        """
        Initializes the Transformer class.

        Args:
            logger: Description of the logger parameter. Defaults to Logger.
        """
        PipelineUtils.__init__(self)
        self.logger = logger()
        self.variables = variables

    def ingest_all_files(
        self,
        spark: SparkSession,
        bucket_id: str,
        blobs: list,
        df_infos: tuple,
    ) -> None:
        """
        Ingestion all files in the given list of blobs.

        Args:
            spark (SparkSession): The SparkSession to use.
            bucket_id (str): The ID of the bucket where the blobs are located.
            blobs (list): The list of blobs to transform.
            df_infos (tuple): Information about the dataframes.
        """
        write_format, delimiter, header, encoding, mode = df_infos
        for blob in blobs:
            if blob.exists() and "part" in blob.name:
                source_path = self.get_source_path(bucket_id, blob)
                # Read CSV file in transient layer.
                df = self.read_csv_file(
                    spark, source_path, delimiter, header, encoding, self.get_schema
                )
                if self.df_is_valid(df):
                    dest_path = source_path.replace("transient", "trusted")
                    # Write a blob into trusted layer.
                    self.write_file(df, write_format, dest_path, mode)

    def main(self):
        """
        The main method of the Loader class.
        """
        STEP_ID, BUCKET_ID, DATASET_ID, PROJECT_ID, APP_NAME, MASTER = self.getenv_vars
        infos = self.variables.LOADER_TASKS.get(STEP_ID)

        dir_name, table_infos, df_params = (infos.dir_name, infos.table_infos, infos.df_params)

        spark = self.init_sparksession(MASTER, APP_NAME, BUCKET_ID, DATASET_ID, PROJECT_ID)

        self.logger.info("Searching for files for ingestion.")
        bucket = self.get_bucket_obj(PROJECT_ID, BUCKET_ID)
        blobs = self.get_list_blobs(bucket, f"transient/{dir_name}")

        self.logger.info("Building Spark Schema for DataFrames.")
        self.build_df_schema(table_infos)

        self.logger.info(f"Running data ingetion for: gs://{BUCKET_ID}/trusted/{dir_name}")
        self.ingest_all_files(spark, BUCKET_ID, blobs, df_params)

        spark.stop()
        self.logger.info("The SparkSession has been stopped.")


if __name__ == "__main__":
    logger = Logger()

    stopwatch = Stopwatch()
    stopwatch.start()

    logger.info("Starting ingestion process.")
    loader = Loader()
    loader.main()

    stopwatch.stop()
    stopwatch.show_elapsed_time()
