# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: strategy.py
# Author: Fernando Theodoro Guimarães
# Description:
# ================================================================================================
from abc import ABC
from abc import abstractmethod

import aiohttp


class Strategy(ABC):
    @abstractmethod
    def create_blob_staging_path(self, page: int) -> str:
        pass

    @abstractmethod
    def process_df_from_json(self, data: dict, blob_name: str) -> None:
        pass

    @abstractmethod
    async def process_response(self, session: aiohttp.ClientSession, page: int) -> int:
        pass

    @abstractmethod
    async def scraper(self, connector) -> None:
        pass
