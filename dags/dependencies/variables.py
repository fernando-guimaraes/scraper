# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: variables.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for configuring the Dataproc Cluster settings.
# ================================================================================================
# pylint: disable=no-member,too-many-public-methods

class Variables:
    def __init__(self, pipeline_config: dict) -> None:
        """
        Initializes the Variables class.

        Args:
            pipeline_config (dict): The configuration for the pipeline.
        """
        self.pipeline_config = pipeline_config
        self.set_attributes()

    def set_attributes(self) -> None:
        """
        Sets the attributes of the class instance based on the keys and values.
        """
        for name, value in self.pipeline_config.items():
            setattr(self, name, value)

    @property
    def get_autoscaling_policy_id(self):
        return self.AUTOSCALING_POLICY_ID

    @property
    def get_bucket_id(self):
        return self.BUCKET_ID

    @property
    def get_bucket_init_actions_id(self):
        return self.BUCKET_INIT_ACTIONS_ID

    @property
    def get_cluster_id(self):
        return self.CLUSTER_ID

    @property
    def get_dataset_id(self):
        return self.DATASET_ID

    @property
    def get_disk_size_gb(self):
        return self.DISK_SIZE_GB

    @property
    def get_disk_type(self):
        return self.DISK_TYPE

    @property
    def get_email(self):
        return self.EMAIL

    @property
    def get_image_version(self):
        return self.IMAGE_VERSION

    @property
    def get_machine_type_uri(self):
        return self.MACHINE_TYPE_URI

    @property
    def get_master_num_instances(self):
        return self.MASTER_NUM_INSTANCES

    @property
    def get_network_id(self):
        return self.NETWORK_ID

    @property
    def get_pip_packages(self):
        return self.PIP_PACKAGES

    @property
    def get_project_id(self):
        return self.PROJECT_ID

    @property
    def get_region(self):
        return self.REGION

    @property
    def get_service_account(self):
        return self.SERVICE_ACCOUNT

    @property
    def get_spark_app_name(self):
        return self.SPARK_APP_NAME

    @property
    def get_spark_master(self):
        return self.SPARK_MASTER

    @property
    def get_subnetwork_id(self):
        return self.SUBNETWORK_ID

    @property
    def get_tmp_dir(self):
        return self.TMP_DIR

    @property
    def get_worker_num_instances(self):
        return self.WORKER_NUM_INSTANCES

    @property
    def get_zone(self):
        return self.ZONE
