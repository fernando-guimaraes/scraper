# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: cluster_config.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for configuring the Dataproc Cluster settings.
# ================================================================================================
# pylint: disable=no-member
from variables import Variables
from google.cloud import dataproc_v1 as dataproc


class ClusterConfig(Variables):
    """
    This class is used to configure a cluster.
    """

    def __init__(self, pipeline_config: dict) -> None:
        """
        Initializes the ClusterConfig class.

        Args:
            pipeline_config (dict): The configuration for the pipeline.
        """
        self.pipeline_config = pipeline_config
        super().__init__(self.pipeline_config)

    def get_gce_cluster_config(self) -> dataproc.types.GceClusterConfig:
        """
        Retrieves the Google Compute Engine (GCE) cluster configuration.

        Returns:
            dataproc.types.GceClusterConfig: The GCE cluster configuration.
        """
        subnetwork_name = (
            "https://www.googleapis.com/compute/v1/projects/{}/regions/{}/subnetworks/{}"
        )
        return dataproc.types.GceClusterConfig(
            metadata={"PIP_PACKAGES": self.get_pip_packages},
            zone_uri=self.get_zone,
            service_account=self.get_service_account,
            service_account_scopes=["https://www.googleapis.com/auth/cloud-platform"],
            subnetwork_uri=subnetwork_name.format(
                self.get_network_id, self.get_region, self.get_subnetwork_id
            ),
            internal_ip_only=True,
        )

    def get_initialization_actions(self) -> dataproc.types.NodeInitializationAction:
        """
        Retrieves the initialization actions for the cluster.

        Returns:
            dataproc.types.NodeInitializationAction: The initialization actions for the cluster.
        """
        return [
            dataproc.types.NodeInitializationAction(
                executable_file=f"gs://{self.get_bucket_init_actions_id}",
                execution_timeout="500s",
            )
        ]

    def get_master_config(self) -> dataproc.types.InstanceGroupConfig:
        """
        Retrieves the master configuration for the cluster.

        Returns:
            dataproc.types.InstanceGroupConfig: The master configuration for the cluster.
        """
        return dataproc.types.InstanceGroupConfig(
            num_instances=int(self.get_master_num_instances),
            machine_type_uri=self.get_machine_type_uri,
            disk_config={
                "boot_disk_type": self.get_disk_type,
                "boot_disk_size_gb": int(self.get_disk_size_gb),
                "num_local_ssds": 0,
                "local_ssd_interface": "SCSI",
            },
        )

    def get_worker_config(self) -> dataproc.types.InstanceGroupConfig:
        """
        Retrieves the worker configuration for the cluster.

        Returns:
            dataproc.types.InstanceGroupConfig: The worker configuration for the cluster.
        """
        return dataproc.types.InstanceGroupConfig(
            num_instances=int(self.get_worker_num_instances),
            machine_type_uri=self.get_machine_type_uri,
            disk_config={
                "boot_disk_type": self.get_disk_type,
                "boot_disk_size_gb": int(self.get_disk_size_gb),
                "num_local_ssds": 0,
                "local_ssd_interface": "SCSI",
            },
        )

    def get_software_config(self) -> dataproc.types.SoftwareConfig:
        """
        Retrieves the software configuration for the cluster.

        Returns:
            dataproc.types.SoftwareConfig: The software configuration for the cluster.
        """
        return dataproc.types.SoftwareConfig(
            image_version=self.get_image_version,
            properties={
                "dataproc:dataproc.allow.zero.workers": "true",
            },
        )

    def get_autoscaling_config(self) -> dataproc.types.AutoscalingConfig:
        """
        Retrieves the autoscaling configuration for the cluster.

        Returns:
            dataproc.types.AutoscalingConfig: The autoscaling configuration for the cluster.
        """
        policy_name = "projects/{}/regions/{}/autoscalingPolicies/{}"
        return dataproc.types.AutoscalingConfig(
            policy_uri=policy_name.format(
                self.get_project_id, self.get_region, self.get_autoscaling_policy_id
            )
        )

    def get_cluster_config(self) -> dataproc.types.ClusterConfig:
        """
        Retrieves the cluster configuration.

        Returns:
            dataproc.types.ClusterConfig: The cluster configuration.
        """
        return dataproc.types.ClusterConfig(
            config_bucket=self.get_bucket_id,
            gce_cluster_config=self.get_gce_cluster_config(),
            master_config=self.get_master_config(),
            worker_config=self.get_worker_config(),
            software_config=self.get_software_config(),
            initialization_actions=self.get_initialization_actions(),
            autoscaling_config=self.get_autoscaling_config(),
        )
