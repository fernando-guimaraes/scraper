# -*- coding: utf-8 -*-
# !/usr/bin/env python3
# ================================================================================================
# Module: dag_utils.py
# Author: Fernando Theodoro Guimarães
# Description: This module is responsible for supporting the construction of the DAG
# ================================================================================================
import uuid
from typing import NamedTuple

from variables import Variables
from google.cloud import storage

# pylint: disable=no-member


class JobParams(NamedTuple):
    """
    JobParams NamedTuple

    This NamedTuple is used to store job information.

    Args:
        step (str): The step name for the job.
        job_id (str): The unique identifier for the job.
        python_file_uris (list): The list of URIs of the Python files.
        main_python_file_uri_extract (str): The URI of the main extract Python file for the job.
        main_python_file_uri_transform (str): The URI of the main transform Python file for the job.
        main_python_file_uri_load (str): The URI of the main load Python file for the job.
    """

    step: str
    job_id: str
    python_file_uris: list
    main_python_file_uri_extract: str
    main_python_file_uri_transform: str
    main_python_file_uri_load: str


class DagUtils(Variables):
    """
    This class is used to helper a DAG.
    """

    def __init__(self, pipeline_config: dict) -> None:
        """
        Initializes the PipelineUtils class.

        Args:
            pipeline_config (dict): The configuration for the pipeline.
        """
        self.pipeline_config = pipeline_config
        self.id = uuid.uuid4()
        super().__init__(self.pipeline_config)

    def get_bucket_obj(self):
        """
        Gets the bucket object from the storage client.

        Returns:
            The bucket object.
        """
        client = storage.Client(self.get_project_id)
        bucket = client.bucket(self.get_bucket_id)
        return bucket

    @staticmethod
    def ends_name_is_py(blob: storage.Blob) -> bool:
        """
        Checks if the blob name starts with "scripts" and ends with ".py", and does not end with "__init__.py".

        Args:
            blob (storage.Blob): The blob to check.

        Returns:
            bool: True if the conditions are met, False otherwise.
        """
        return (
            blob.name.startswith("scripts")
            and blob.name.endswith(".py")
            and not blob.name.endswith("__init__.py")
        )

    def get_python_file_uris(self, blob_list: list) -> list:
        """
        Gets the URIs of the Python files in the blob list.

        Args:
            blob_list (list): The list of blobs.

        Returns:
            list: The list of URIs of the Python files.
        """
        return [
            f"gs://{self.get_bucket_id}/{blob.name}"
            for blob in blob_list
            if self.ends_name_is_py(blob)
        ]

    def get_list_blobs(self, prefix: str = None) -> list:
        """
        Gets a list of blobs in the bucket that match the given prefix.

        Args:
            prefix (str, optional): The prefix to match. Defaults to None.

        Returns:
            list: A list of blobs that match the prefix.
        """
        bucket = self.get_bucket_obj()
        return list(bucket.list_blobs(prefix=prefix))

    def task_parameters_map(self) -> dict:
        """
        Gets the tasks for the pipeline.

        Returns:
            dict: The tasks for the pipeline.
        """
        BLOBS = self.get_list_blobs(prefix="scripts/src")
        PYTHON_FILE_URIS = self.get_python_file_uris(BLOBS)
        return {
            "conselho-federal-contabilidade": JobParams(
                step="{}",
                job_id=f"""cfc-{{}}-{self.id}""",
                python_file_uris=PYTHON_FILE_URIS,
                main_python_file_uri_extract=f"gs://{self.get_bucket_id}/scripts/src/pipeline/extract/scrapers"
                f"/conselho_federal_contabilidade.py",
                main_python_file_uri_transform=f"gs://{self.get_bucket_id}/scripts/src/pipeline/transform"
                f"/main_transformer.py",
                main_python_file_uri_load=f"gs://{self.get_bucket_id}/scripts/src/pipeline/load/main_loader.py",
            )
        }
