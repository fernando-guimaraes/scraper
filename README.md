# PROFISSIONAIS AUTÔNOMOS

## 1. Objetivo

## 2. Solução Proposta

## 3. Instalação

### Clone o repositório:

```bash
git clone git@gitlab.com:fernando-guimaraes/scraper.git
cd scraper
```

### Crie, ative e instale em um ambiente virtual:

Mac/Linux:

```bash
python -m venv <your-venv>
source <your-venv>/bin/activate
pip install -r requirements.txt
```

Windows:

```bash
python -m venv <your-venv>
<your-venv>\Scripts\activate
pip install -r requirements.txt
```

## 4. Implementação

### Diagrama de Arquitetura


### DAG
![DAG Pipeline](imgs/DAG.png)


## 5. Descrição do Processo

## 6. Estrutura do Projeto
Nesta seção é descrito o funcionamento de cada um dos módulos da aplicação.

------------
    ├── imgs <-
    ├── src <- Código fonte para uso neste projeto.
    │   ├── pipeline <- Scripts para execução das etapas de ETL.
    │   │   ├── extract <- Scripts para extração dos dados.
    │   │   │   ├── scraping <- Scripts que dão suporte para a captura dos dados. 
    │   │   ├── load <- Scripts para ingestão dos dados.
    │   │   ├── transform <- Scripts para processamento e transformação dos dados.
    │   ├──tests <- Scripts para executar testes automatizados.
    │   │   ├── integration <-
    │   │   ├── unit <-
    │   ├── utils <- Scripts para dar suporte aos demais módulos do projeto.
    │   │   ├── ci <-
    │   │   ├── dataframe <-
    │   │   ├── gcp <- Scripts para interagir com recursos do GCP.
    │   │   ├── helpers <-
    │   │   ├── request <-
    │   │   ├── spark <-
    │   │   ├── time <-
    ├── .dockerignore <- Script que lista padrões de arquivos e diretórios a serem ignorados pelo Docker.
    ├── .gitignore <- Script que lista padrões de arquivos e diretórios a serem ignorados pelo Git durante o controle de versão.
    ├── .gitlab-ci.yml <- Script YAML que especifica as instruções para GitLab CI/CD.
    ├── .pre-commit-config.yml <-
    ├── .pylintrc <-
    ├── Dockerfile <- Script Docker para construir o contêiner da aplicação.
    ├── main.py <- Script principal a ser executado pelo Cloud Function.
    ├── README.md <- O README de nível superior para desenvolvedores que usam este projeto.
    ├── requirements.txt <- O arquivo de requisitos para reproduzir o ambiente de análise, e.g. gerado com o pip freeze > requirements.txt
    ├── setup.cfg <- Script de configuração usado bibliotecas de análise estática.
    ├── setup.py <- Script de configuração usado para instalar, empacotar e distribuir o projeto Python.
--------


## 7. Utilização


## 8. Documentações úteis